#!/bin/bash

usage()
{
    echo "Usage: ./sudoku-to-png.sh [OPTION]... [FILE]"
    echo "Convert sudoku in text format into PNG image"
    echo "Options:"
    echo -e "  -o, --output\t\t: name of output image"
    echo -e "  -w, --width\t\t: width of an output image"
    echo -e "  -k, --keep-svg\t: don't delete temporary SVG file"
    echo -e "  -h, --help\t\t: display help and exit"
    echo ""
}

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -w|--width)
    WIDTH="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    OUTFILE="$2"
    shift # past argument
    shift # past value
    ;;
    -k|--keep-svg)
    KEEPSVG=true
    shift
    ;;
    -h|--help)
    usage
    exit
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ -n "$POSITIONAL" ]; then
    INFILE=$POSITIONAL
else
    echo "Input file not specified."
    exit
fi

if [ -z "$OUTFILE" ]; then
    OUTFILE=${INFILE%.*}.png
fi

TEMPFILE=${INFILE%.*}.svg
python3 ./sudoku-to-svg.py $INFILE $TEMPFILE || exit

INKSCAPE=$(which inkscape 2> /dev/null)

if [ -n "$INKSCAPE" ]; then
    if [[ -n "$WIDTH" ]] && [[ "$WIDTH" -gt 0 ]]; then
        inkscape -z -f $TEMPFILE -w $WIDTH -j -e $OUTFILE
    else
        inkscape -z -f $TEMPFILE -j -e $OUTFILE
    fi

    if [ "$KEEPSVG" = true ]; then
        echo "Temporary file saved as:" $TEMPFILE
    else
        rm $TEMPFILE
    fi
else
    echo "Inkscape not found."
    echo "At least you have SVG" $TEMPFILE
fi
exit
