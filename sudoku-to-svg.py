import svgwrite

def add(t1, t2):
    return tuple(x+y for x, y in zip(t1, t2))

SIZE = 9
BLOCK_SIZE = 3
RECT_SIZE = 50
FONT_SIZE = 26
STROKE = 2
HSTROKE = 8
SCOLOR = '#000000'
BCOLOR = '#ffffff'
FCOLOR = '#000000'

dwg_size = (RECT_SIZE*SIZE+HSTROKE+STROKE,) * 2
font_offset = (RECT_SIZE/2,RECT_SIZE/2 + STROKE)
big_offset = (HSTROKE/2,) * 2
big_size = (RECT_SIZE*BLOCK_SIZE + STROKE,) * 2

def parse_char(char):
    try:
        return int(char)
    except:
        return ''

def parse_line(line):
    row = list(map(parse_char, line.strip().split()[:SIZE]))
    return row + [''] * (SIZE - len(row))

def read_file(filename):
    try:
        with open(filename, 'r') as f:
            sudoku = list(map(parse_line, f))[:SIZE]
        return sudoku + [[''] * SIZE] * (SIZE - len(sudoku))
    except:
        print('Input file cannot be read.')
        exit(1)


def create_svg(filename, outfile):
    sudoku = read_file(filename)
    dwg = svgwrite.Drawing(outfile, dwg_size, font_size=FONT_SIZE)
    # Big sqares
    for row in range(BLOCK_SIZE):
        for col in range(BLOCK_SIZE):
            position = add((col*BLOCK_SIZE*RECT_SIZE, row*BLOCK_SIZE*RECT_SIZE), big_offset)
            style = { 'fill': 'none', 'stroke_width': HSTROKE, 'stroke': SCOLOR}
            dwg.add(dwg.rect(position, big_size, **style))
    # Individual cells
    for row, r in enumerate(sudoku):
        for col, c in enumerate(r):
            offset = (STROKE/2 + HSTROKE/4 * (1 + (col//BLOCK_SIZE)), STROKE/2 + HSTROKE/4 * (1+ (row//BLOCK_SIZE)))
            position = add((col*RECT_SIZE, row*RECT_SIZE), offset)
            font_position = add(position, font_offset)
            size = (RECT_SIZE,) * 2
            style = { 'fill': BCOLOR, 'stroke_width': STROKE, 'stroke': SCOLOR}
            dwg.add(dwg.rect(position, size, **style))
            dwg.add(dwg.text(c, insert=font_position, dominant_baseline='middle', text_anchor='middle', fill=FCOLOR))
    dwg.save()

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 2:
        create_svg(sys.argv[1], sys.argv[2])
    else:
        print('Not enough arguments.')
        exit(1)
