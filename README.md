# sudoku-to-png
This is a tiny script that generates a PNG image of sudoku that looks like this

![Sudoku](./example.png)

from this input file

```
. . 8 . . 6 . . .
. . . . . . . . 1
. . 4 7 5 . . . 6
1 . . . 3 5 . . .
. . . 2 . . . . 7
3 . . . . . . 8 4
. . . . . . . 1 .
. . 5 6 . 2 9 . 8

```

## Installation
1. Instal [**svgwrite**](https://pypi.org/project/svgwrite/) library

```shell
pip install -r requirements.txt --user
```

2. For PNG output you need [**inkscape**](https://inkscape.org/) as well

```shell
sudo pacman -S inkscape
```

3. That's it!

## Usage
```shell
Usage: ./sudoku-to-png.sh [OPTION]... [FILE]
Convert sudoku in text format into PNG image
Options:
  -o, --output		: name of output image
  -w, --width		: width of an output image
  -k, --keep-svg	: don\'t delete temporary SVG file
  -h, --help		: display help and exit

```
